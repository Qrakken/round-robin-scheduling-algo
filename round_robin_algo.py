def w_t(p,n,bt,wt,q):   # n = no of proccesses, bt = burst time, wt = waiting time
    remain_bt = [0] * n 
    
    for i in range(n):   # copy the burst time to remain_bt[]
        remain_bt[i] = bt[i]
    
    curr_t = 0      # current time 

    
    while(1):       #traversing of all the processes is done here in RR until all the procrsses are not finished.
        p_finish = True

        for i in range(n):
            if (remain_bt[i] > 0):
                p_finish = False    # A proccess is not finished yet.
                
                if (remain_bt[i] > q):
                    curr_t += q     #increasing current time shows how 
                    remain_bt[i] -= q  #decreasing remain_bt by quantum 

                else:                   #if bt <= quantum
                    curr_t += remain_bt[i]
                    wt[i] = curr_t - bt[i]
                    remain_bt[i] = 0

        if (p_finish == True):   # when all the processes are finished 
            break   #to break the loop 


def t_a_t(p,n,bt,wt,tat):   #to calculate Turn arround time
    for i in range(n):
        tat[i] = bt[i] + wt[i]  # sum of burst time and waiting time gives turn around time



def avg_time(p,n,bt,q):
    wt = [0] * n     #initializing array of waiting time
    tat = [0]* n     #initializing array of turn arround time

    w_t(p,n,bt,wt,q) #calling w_t function
    t_a_t(p,n,bt,wt,tat) #calling t_a_t function

    print("Processes","   Burst Time","    Waiting Time", "    Turn Around Time")

    total_wt = 0 
    total_tat = 0

    for i in range(n):
        total_wt += wt[i]
        total_tat += tat[i]

        print(" ", i +1,"\t\t",bt[i],"\t\t",wt[i],"\t\t",tat[i])

    print("\nAverage Waiting Time = %.3f "%(total_wt/n)) 
    print("Average Turn Around Time = %.3f "%(total_tat/n))


if __name__ == "__main__":
    process_array = list()
    '''User can enter desired number of processes and burst time of each processes'''

    number = input("Enter the number of processes: ")
    print("Enter burst time of each processes in array:")
    for i in range(int(number)):
        n = input("burst time: ")
        process_array.append(int(n))
    
    processes = int(number)
    burst_time = process_array
    n = int(number)
    q = 2
    avg_time(processes,n, burst_time,q)
    print("Quantum = ",q)
